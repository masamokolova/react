import React from "react";
import "./App.css";
import Modal from "./Modal";
import { useState } from "react";

function App() {
  const [modal, setModal] = useState({
    modal1: false,
    modal2: false,
  });
  return (
    <div className="app">
      <button
        className="double-border-button"
        onClick={() =>
          setModal({
            ...modal,
            modal1: true,
          })
        }
      >
        Open first modal
      </button>

      <button
        className="double-border-button"
        onClick={() =>
          setModal({
            ...modal,
            modal2: true,
          })
        }
      >
        Open second modal
      </button>

      <Modal
        title={"Delete your Account?"}
        text={"This action is final and you will be unable to recover any data"}
        isOpened={modal.modal1}
        onModalClose={() => setModal({ ...modal, modal1: false })}
      ></Modal>

      <Modal
        title={"Modal"}
        text={
          "The content of the modal window. Can be anything you like and text and form fields and graphics, etc."
        }
        isOpened={modal.modal2}
        onModalClose={() => setModal({ ...modal, modal2: false })}
      ></Modal>
    </div>
  );
}

export default App;
