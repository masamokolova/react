// import React from "react";
import "./modal.css";

const Modal = (props) => {
  return (
    <div className={`modal modal-content ${props.isOpened ? "open" : "close"}`}>
      <div className="modal__content-header">
        <header className="modal-header">{props.title}</header>
        <div className="modal__close " onClick={props.onModalClose}>
          X
        </div>
      </div>
      <div className="modal__content-main">{props.text}</div>
      <div className="modal-btn-flex">
        <button className="modal-buttton">Yes</button>
        <button className="modal-buttton">No</button>
      </div>
    </div>
  );
};

export default Modal;
