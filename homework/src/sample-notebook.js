const sampleNotebook = {
  notebook1: {
    title: "Ноутбук Asus VivoBook 15",
    price: 19499,
    url: "notebook-asus-vivoBook_15",
    color: "gray",
    image: "/img/asus_vivoBook15.jpg",
  },

  notebook2: {
    title: "Ноутбук Lenovo IdeaPad 3",
    price: 18999,
    url: "notebook-lenovo-ideaPad_3",
    color: "gray",
    image: "/img/lenovo_ideaPad_3.jpg",
  },

  notebook3: {
    title: "Ноутбук HP Laptop 15s",
    price: 18399,
    url: "notebook-hp-laptop_15s",
    color: "white",
    image: "/img/hp-laptop_15s.jpg",
  },

  notebook4: {
    title: "Ноутбук Apple MacBook Air 13",
    price: 33999,
    url: "notebook-apple-macBook-air_13",
    color: "gold",
    image: "/img/apple_macbook_air_13.jpg",
  },

  notebook5: {
    title: "Ноутбук ACER Aspire 3",
    price: 15999,
    url: "notebook-ACER-Aspire_3",
    color: "black",
    image: "/img/acer-aspire_3.jpg",
  },

  notebook6: {
    title: "Ноутбук Samsung Galaxy Book",
    price: 591518,
    url: "notebook-sansung-galaxy-book-flex",
    color: "white",
    image: "/img/Galaxy-Book-Ion2.jpg",
  },

  notebook7: {
    title: "Ноутбук Mi RedmiBook 14",
    price: 26999,
    url: "notebook-mi-redmiBook_14",
    color: "gray",
    image: "/img/mi-redmi_14.jpg",
  },

  notebook8: {
    title: "Ноутбук Microsoft Surface Go",
    price: 30499,
    url: "notebook-Microsoft-Surface-Laptop-Go",
    color: "gold",
    image: "/img/microsoft.jpg",
  },

  notebook9: {
    title: "Ноутбук Huawei MateBook 15",
    price: 323999,
    url: "notebook-Huawei-MateBook_15",
    color: "black",
    image: "/img/huawei-mateBook-d15.jpg",
  },

  notebook10: {
    title: "Ноутбук Dell Vostro 15",
    price: 21319,
    url: "notebook-Dell-Vostro_15",
    color: "black",
    image: "/img/dell-g3.jpg",
  },

  notebook11: {
    title: "Ноутбук MSI Modern 14",
    price: 20999,
    url: "notebook-MSI-Modern_14",
    color: "gray",
    image: "/img/msi-modern-14.jpg",
  },

  notebook12: {
    title: "Ноутбук RAZER PRO 17",
    price: 71611,
    url: "notebook-apple-macBook-air_13",
    color: "black",
    image: "/img/razer.jpg",
  },
};

export default sampleNotebook;
