import React from "react";
import Header from "./Header";
import Order from "./Order";
import CatalogAdmin from "./CatalogAdmin";
import sampleNotebook from "../sample-notebook";
import Notebook from "./Notebook";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import "../css/reset.css";
import "../css/menuNotebook.css";
import "../css/notebookProduct.css";
import "../css/order.css";
import "../css/Header.css";
import Home from "./Home";
import Basket from "./Basket";
import Favorites from "./Favorites";
// import base from "../base";
class App extends React.Component {
  state = {
    notebooks: JSON.parse(localStorage.getItem("notebooksArr")) || {},
    order: JSON.parse(localStorage.getItem("saveOrder")) || {},
  };

  // componentDidMount() {
  //   console.log("mount");
  // }

  loadSampleNotebook = () => {
    this.setState({ notebooks: sampleNotebook });
    localStorage.setItem("notebooksArr", JSON.stringify(sampleNotebook));
  };

  deleteFromOrder = (key) => {
    const order = { ...this.state.order };
    delete order[key];
    this.setState({ order });
    localStorage.setItem("saveOrder", JSON.stringify(order));
  };
  addToOrder = (key) => {
    const order = { ...this.state.order };
    order[key] = order[key] + 1 || 1;
    this.setState({ order });
    localStorage.setItem("saveOrder", JSON.stringify(order));
  };
  deleteNotebook = (key) => {
    const notebooks = { ...this.state.notebooks };
    notebooks[key] = null;
    this.setState({ notebooks });
  };
  render() {
    return (
      <div className="notebook-paradise">
        <div className="catalog">
          <Header />
          <Router>
            <ul type="disc" className="rounded">
              <li>
                <a href="/">Main</a>
              </li>
              <li>
                <a href="/basket">Basket</a>
              </li>
              <li>
                <a href="/favorites">Favorites</a>
              </li>
              {/* <li>
                <a href="/favorite">Bask</a>
              </li> */}
            </ul>
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/basket" component={Basket} />
              <Route exact path="/favorites" component={Favorites} />
              {/* <Route exact path="/favorite" component={CatalogAdmin} /> */}
            </Switch>
          </Router>
          <ul className="notebook-list">
            {Object.keys(this.state.notebooks).map((key) => {
              return (
                <Notebook
                  key={key}
                  index={key}
                  addToOrder={this.addToOrder}
                  details={this.state.notebooks[key]}
                />
              );
            })}
          </ul>
        </div>
        <CatalogAdmin
          loadSampleNotebook={this.loadSampleNotebook}
        ></CatalogAdmin>
        <Order
          deleteFromOrder={this.deleteFromOrder}
          notebooks={this.state.notebooks}
          order={this.state.order}
        />
      </div>
    );
  }
}
export default App;
