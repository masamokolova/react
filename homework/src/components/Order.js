import React from "react";

class Order extends React.Component {
  renderOrder = (key) => {
    const notebook = this.props.notebooks[key];
    const count = this.props.order[key];

    return (
      <li>
        <span>
          <span>{count}</span>
          шт. {notebook.title}
          <span> {count * notebook.price} ₴ </span>
          <button
            onClick={() => this.props.deleteFromOrder(key)}
            className="cancelItem"
          >
            &times;
          </button>
        </span>
      </li>
    );
  };

  render() {
    const orderIds = Object.keys(this.props.order);
    const total = orderIds.reduce((prevTotal, key) => {
      const notebook = this.props.notebooks[key];
      const count = this.props.order[key];

      return prevTotal + notebook.price * count;
    }, 0);

    return (
      <div className="order-wrap">
        <h2>Ваш заказ</h2>
        <ul  className="order">{orderIds.map(this.renderOrder)}</ul>
        <div className="total">
          <div className="total_wrap">
            <div className="total_wrap-final">Итого: {total} ₴</div>
          </div>
        </div>
      </div>
    );
  }
}
export default Order;
