import React from "react";
import notebook from "../sample-notebook";
function Landing() {
    return notebook.map((notebook) => {
      return <li key={notebook.price}>{notebook.title}</li>;
    });
  }

export default Landing;
