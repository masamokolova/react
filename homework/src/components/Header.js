import React from "react";
import "../css/Header.css";

const Header = (props) => (
  <header className="top">
    <div className="wrap">
      <div className="header-content">
        <div className="header-rating">
          <div className="heading-rating_icon">
            <i className="fas fa-laptop"></i>
          </div>
          <div className="header-taring-tag">electro</div>
        </div>

        <div className="heading-divider"></div>
        <button className="button-header">Каталог</button>
        <p>
          <input className="input-header" type="submit" value="Я ищу"></input>
        </p>
      </div>
    </div>
  </header>
);
export default Header;
