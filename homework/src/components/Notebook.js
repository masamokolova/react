import React from "react";
import { Redirect } from "react-router";
import "../css/modal.css";
import "../css/notebookProduct.css";
class Notebook extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      flag: JSON.parse(localStorage.getItem("chandeColor")) || false,
    };
  }
  state = {
    isOpen: false,
  };

  changeColor = () => {
    const flag = { ...this.state.flag };
    this.setState({
      flag: !this.state.flag,
    });

    localStorage.setItem("chandeColor", JSON.stringify(flag));
  };

  handleClick = () => {
    this.setState({
      isOpen: !this.state.isOpen,
    });
  };
  addBasket = () => {
    this.props.addToOrder(this.props.index);
    this.setState({
      isOpen: !this.state.isOpen,
    });
  };
  doNotAddBasket = () => {
    this.setState({
      isOpen: !this.state.isOpen,
    });
  };

  render() {
    const Modal = (props) => {
      return (
        <div className="modal modal-content ">
          <div className="modal__content-header">
            <header className="modal-header">{props.title}</header>
            <div className="modal__close " onClick={this.handleClick}>
              X
            </div>
          </div>
          <div className="modal__content-main">{props.text}</div>
          <div className="modal-btn-flex">
            <button className="modal-buttton" onClick={this.addBasket}>
              Yes
            </button>

            <button className="modal-buttton" onClick={this.doNotAddBasket}>
              No
            </button>
          </div>
        </div>
      );
    };
    const modal = this.state.isOpen && (
      <Modal
        title={"Attention"}
        text={"Add a product to your shopping cart?"}
      />
    );

    const { image, title, price, color } = this.props.details;
    return (
      <li className="menu-notebook">
        <div className="image-container">
          <img className="image-notebook" src={image} alt={title} />
        </div>
        <div className="notebook-details">
          <h3 className="notebook-name">{title}</h3>
          <div
            className="clickLike"
            style={{ color: this.state.flag === true ? "orange" : "black" }}
            onClick={this.changeColor}
          >
            ★★★★★
          </div>

          <p className="colorNotebook">Цвет: {color}</p>
          <div className="notebookProductBottom">
            <span className="price">{price}₴ </span>
            <button className="btn" onClick={this.handleClick}>
              В корзину
              <i className="fas fa-shopping-cart"></i>
            </button>
            <div>{modal}</div>
          </div>
        </div>
      </li>
    );
  }
}
export default Notebook;
