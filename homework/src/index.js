import React from "react";
import { render } from "react-dom";
// import { BrowserRouter, Switch, Route } from "react-router-dom";
import App from "./components/App";

// import Header from "./components/Header"

// render(
//   <BrowserRouter>
//     <App>
//       <Switch>
//         <Route exact  path="/" component={Header} />
//       </Switch>
//     </App>
//   </BrowserRouter>,

//   document.getElementById("root")
// );
render(<App />, document.getElementById("root"))